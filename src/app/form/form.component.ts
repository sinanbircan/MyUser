import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {UserService} from '../user.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {

  model = new User('', '', '', '', '');

  constructor(private userService: UserService) { }

  /**
   * checks whether given email input includes an '@' and after that a '.'
   * @returns {boolean}
   */
  emailValidator() {
    if (!this.model.email.includes('@')) {
      return true;
    } else {
      const indexOfAt = this.model.email.indexOf('@');
      const rest = this.model.email.substr(indexOfAt);
      return !(rest.includes('.'));
    }
  }

  /**
   * sends the form data to user service for registration
   */
  onSubmit() {
    this.userService.addUser(this.model);
  }
}
