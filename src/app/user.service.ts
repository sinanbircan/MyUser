import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable, of} from 'rxjs/index';
import {User} from './user';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  usersUrl = 'assets/users.json';
  registrationSuccess = false;

  constructor(
    private http: HttpClient) { }

  /**
   * makes a request to the assets folder to get the users.json file.
   * returns the observable User[] when request completes.
   * @returns {Observable<User[]>}
   */
  getUsers (): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl);
  }

  /**
   * takes the new user and adds it to the localstorage.
   * @param {Object} user
   */
  addUser (user: object) {
    let currentUserList = JSON.parse(localStorage.getItem('users'));
    currentUserList.push(user);
    localStorage.setItem('users', JSON.stringify(currentUserList));
    this.registrationSuccess = true;
  }

  /**
   * resets registrationSuccess variable to prevent info component to be displayed
   * while adding a new user.
   */
  resetRegistrationSuccess() {
    this.registrationSuccess = false;
  }
}
