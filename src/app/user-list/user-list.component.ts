import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {UserService} from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: User[];
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUsers();
  }

  /**
   * if localstorage is empty, it calls userservice.getUsers to get the json file.
   * else it returns the array from the localstorage.
   */
  getUsers(): void {
    const usersFromLocalStorage = localStorage.getItem('users');
    if (usersFromLocalStorage) {
      this.users = JSON.parse(usersFromLocalStorage);
    } else {
      this.userService.getUsers()
        .subscribe(users => {
          localStorage.setItem('users', JSON.stringify(users));
          this.users = users;
        });
    }
  }

}
