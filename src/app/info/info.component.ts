import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {
  // userService is defined because UserService.registrationSuccess is used in the html file
  constructor(private userService: UserService) { }

  ngOnInit() {
  }
}
